<?php

require_once __DIR__ . "/lib/vendor/autoload.php";

function setLogging ($debug = false) {
    ini_set("display_errors", intval($debug));
    ini_set("display_startup_errors", intval($debug));
    error_reporting(E_ALL);
}

function setLocalization ($locale) {
    $path = __DIR__ . "/i18n";
    $domain = "main";

    putenv("LANG=" . $locale);
    setlocale(LC_ALL, $locale);

    $sharedGettext = new \Audero\SharedGettext\SharedGettext($path, $locale, $domain);

    $newDomain = $sharedGettext->updateTranslation();

    bindtextdomain($newDomain, $path);
    bind_textdomain_codeset($newDomain, "UTF-8");

    textdomain($newDomain);
}

setLogging(true);
setLocalization("en_US");

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <?= _('Hello world!') ?>
    </body>
</html>
